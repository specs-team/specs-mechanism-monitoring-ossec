#
# Cookbook Name:: specs-monitoring-ossec
# Recipe:: agent
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# get implementation plan
plan_id = node['implementation_plan_id']
plan = search("implementation_plans","id:#{plan_id}").first

if plan == nil then
puts "Implementation Plan with id: #{plan_id} is not present in Implementation plans data bag"
exit
end

# get ossec server ip address
plan['IaaS']['VMs']['nodes'].each do | node |
  node['recipes'].each do | recipe |
   if (recipe['cookbook']=="specs-monitoring-ossec" and recipe['name']=="server")
   $ossecServer=node['private-ips'].first
   end
  end
end

if $ossecServer == nil 
puts "You need acquire a node to install an ossec server ( recipe ossec::server )"
exit
end

# Download package ossec agent

cookbook_file  "Agent.tar.gz" do
   path "/tmp/ossec-agent.tar.gz"
   action :create
end

# Download installer script

cookbook_file "scripts.tar.gz" do  
   path "/tmp/ossec-scripts.tar.gz"
   action :create
end

# unzip tar file and execute installer script

bash "install_program" do
        user "root"
        cwd  "/opt"
        code <<-EOH

	tar -zxvf /tmp/ossec-agent.tar.gz
	rm /tmp/ossec-agent.tar.gz
	tar -xvf /tmp/ossec-scripts.tar.gz
	rm /tmp/ossec-scripts.tar.gz

	chmod +x ossec-scripts/InstallAgent.sh 
	chmod +x ossec-scripts/InsertAgentKey.sh
	chmod +x ossec-scripts/Installsyslog.sh

	#if syslog is not installed or not running then it is installed and started
	ossec-scripts/Installsyslog.sh
        ossec-scripts/InstallAgent.sh #$ossecServer	

	zypper -n in expect

	rm -r ossec-agent

        EOH
        not_if { ::File.exists?("/opt/ossec-agent") }
end





