#
# Cookbook Name:: specs-monitoring-ossec
# Recipe:: server
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# get implementation plan
plan_id = node['implementation_plan_id']
plan = search("implementation_plans","id:#{plan_id}").first

if plan == nil then
puts "Implementation Plan with id: #{plan_id} is not present in Implementation plans data bag"
exit
end

# get EventHub information
EventHubIP=plan['context']['monitoring-core-ip']
EventHubPort=plan['context']['monitoring-core-port']

if EventHubIP == nil or EventHubPort == nil
puts "Insert monitoring-core-ip and monitoring-core-port in context tag of implementation plan"
exit
end 

# Download package ossec server

cookbook_file  "Server.tar.gz" do
   path "/tmp/ossec-server.tar.gz"
   action :create
end

# Download installer script

cookbook_file "scripts.tar.gz" do
   path "/tmp/ossec-scripts.tar.gz"
   action :create
end

# Download ossec adapter script

cookbook_file "ossec-adapter.js" do  
   path "/opt/ossec-adapter.js"
   action :create
end


# unzip tar file and execute installer script

bash "install_program" do
        user "root"
        cwd  "/opt"
        code <<-EOH 

        tar -zxvf /tmp/ossec-server.tar.gz
	rm /tmp/ossec-server.tar.gz
	tar -xvf /tmp/ossec-scripts.tar.gz
	rm /tmp/ossec-scripts.tar.gz

	chmod +x ossec-scripts/Installsyslog.sh
        #if syslog is not installed or not running then it is installed and started
        ossec-scripts/Installsyslog.sh

        chmod +x ossec-scripts/InstallServer.sh 
        ossec-scripts/InstallServer.sh
	ossec/bin/ossec-control start

        chmod +x ossec-scripts/InstallNodeJs.sh
	ossec-scripts/InstallNodeJs.sh /opt/ossec-adapter.js  #{EventHubIP} #{EventHubPort}

	chmod +x ossec-scripts/ossec-batch-manager.pl
	chmod +x ossec-scripts/AddAgent.sh

	rm -r ossec-server
	EOH
        not_if { ::File.exists?("/opt/ossec-server") }
end

# get ossec agents ip address
plan['IaaS']['VMs']['nodes'].each do | node |
  node['recipes'].each do | recipe |
   if (recipe['cookbook']=="specs-monitoring-ossec" and recipe['name']=="agent")

	name=node['node-id']
        IP_Address=node['private-ips'].first

	# Add Agent
	bash "add_agent" do
        user "root"
        cwd  "/opt"
        code <<-EOH
        ossec-scripts/AddAgent.sh #{name} #{IP_Address}
        ossec/bin/ossec-control restart
        EOH
        end
   end
  end
end


