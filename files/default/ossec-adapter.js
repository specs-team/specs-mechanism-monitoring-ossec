// Procedura per prendere l'indirizzo ip da argomento. Delle volte mi ha bloccato la porta 4880 e non funzionava più il modulo. Nel caso, decommentare tutta la procedura per prendere l'argomento, ed inserire l'indirizzo di HEKA qui in HOSTFWD a mano. Oppure togliere solo process.kill()
var myArgs = process.argv.slice(2);

if(myArgs[0]===undefined){

        console.log("passa l'indirizzo ip di HEKA come argomento\n"+
                'es. "node ossec-heka-converter.js 54.93.30.151 5000"\n');

	process.kill();
}
if(myArgs[1]===undefined){

        console.log("passa port  di HEKA come argomento\n"+
                'es. "node ossec-heka-converter.js 54.93.30.151 5000"\n');

        process.kill();
}

/////////////////////////////////////////////////////////////////////
var PORT = 4880;
var PORTFWD = myArgs[1];
var HOST = '127.0.0.1';
var HOSTFWD = myArgs[0];


var dgram = require('dgram');
var server = dgram.createSocket('udp4');

server.on('listening', function () {
    var address = server.address();
    console.log('UDP Server listening on ' + address.address + ":" + address.port);

	console.log('UDP Server send on ' + HOSTFWD + ":" + PORTFWD);
});


server.on('message', function (message, remote) {
    console.log(remote.address + ':' + remote.port +' - ' + message);
	var client = dgram.createSocket('udp4');
	
	var regexjson = /{.*}/g;
	var regexinfo = /<.*(?={)/g;
	var mseconds = new Date().getTime(); // /1000 per i secondi

	var seljson = regexjson.exec(message)[0];
	var json = JSON.parse(seljson);

	var typefield = new Array();
	for(var i=0; i<=999; i++){
		typefield[i]="OSSEC Internal";
	}

	for(var i=1000; i<=1999; i++){
		typefield[i]="Syslog General";
	}

	for(var i=2100; i<=2299; i++){
		typefield[i]="NetworkFileSystem";
	}

	for(var i=2300; i<=2499; i++){
		typefield[i]="Xinetd";
	}
	
	for(var i=2500; i<=2699; i++){
		typefield[i]="Access Control";
	}

	for(var i=2700; i<=2729; i++){
		typefield[i]="Procmail";
	}

	for(var i=2800; i<=2829; i++){
		typefield[i]="Smartd";
	}

	for(var i=2830; i<=2859; i++){
		typefield[i]="Crond";
	}

	for(var i=2900; i<=2929; i++){
		typefield[i]="Dpkg";
	}

	for(var i=2930; i<=2959; i++){
		typefield[i]="Yum";
	}

	for(var i=3100; i<=3299; i++){
		typefield[i]="Sendmail MS";
	}

	for(var i=3300; i<=3499; i++){
		typefield[i]="Postfix";
	}

	for(var i=3500; i<=3599; i++){
		typefield[i]="Spamd";
	}

	for(var i=3600; i<=3699; i++){
		typefield[i]="Imapd";
	}

	for(var i=3700; i<=3799; i++){
		typefield[i]="Mail Scanner";
	}

	for(var i=3800; i<=3899; i++){
		typefield[i]="MS Exchange Mail";
	}

	for(var i=3900; i<=3999; i++){
		typefield[i]="Courier Mail";
	}

	for(var i=4100; i<=4299; i++){
		typefield[i]="Firewall Generic";
	}

	for(var i=4300; i<=4499; i++){
		typefield[i]="Cisco pic/asa/fwsm";
	}

	for(var i=4500; i<=4699; i++){
		typefield[i]="Juniper Netscreen";
	}

	for(var i=4700; i<=4799; i++){
		typefield[i]="Cisco IOS";
	}

	for(var i=4800; i<=4899; i++){
		typefield[i]="Sonicwall";
	}
		
	for(var i=5100; i<=5299; i++){
		typefield[i]="Linux Kernel";
	}

	for(var i=5300; i<=5399; i++){
		typefield[i]="Switch User (su)";
	}

	for(var i=5400; i<=5499; i++){
		typefield[i]="Sudo";
	}

	for(var i=5500; i<=5599; i++){
		typefield[i]="PAM";
	}

	for(var i=5600; i<=5699; i++){
		typefield[i]="Telnetd";
	}

	for(var i=5700; i<=5899; i++){
		typefield[i]="Sshd";
	}

	for(var i=5900; i<=5999; i++){
		typefield[i]="Add User";
	}

	for(var i=6200; i<=6299; i++){
		typefield[i]="Asterisk";
	}

	for(var i=6300; i<=6399; i++){
		typefield[i]="MS Dhcp";
	}

	for(var i=7100; i<=7199; i++){
		typefield[i]="Tripwire";
	}

	for(var i=7200; i<=7299; i++){
		typefield[i]="Arpwatch";
	}

	for(var i=7200; i<=7299; i++){
		typefield[i]="Symantec AV";
	}

	for(var i=7400; i<=7499; i++){
		typefield[i]="Symantec WS";
	}

	for(var i=7500; i<=7599; i++){
		typefield[i]="Mcafee";
	}

	for(var i=7600; i<=7699; i++){
		typefield[i]="Trend OSCE";
	}

	for(var i=7700; i<=7799; i++){
		typefield[i]="MS Se";
	}
	
	for(var i=9100; i<=9199; i++){
		typefield[i]="PPTP";
	}

	for(var i=9200; i<=9299; i++){
		typefield[i]="Squid Syslog";
	}

	for(var i=9300; i<=9399; i++){
		typefield[i]="Horde IMP";
	}

 	for(var i=9400; i<=9499; i++){
		typefield[i]="RoundCube";
	}

	for(var i=9500; i<=9599; i++){
		typefield[i]="Wordpress";
	}

	for(var i=9600; i<=9699; i++){
		typefield[i]="Cimserver";
	}
	
	for(var i=9700; i<=9799; i++){
		typefield[i]="Dovecot";
	}
	
	for(var i=9800; i<=9899; i++){
		typefield[i]="VMpop3d";
	}

	for(var i=9900; i<=9999; i++){
		typefield[i]="Vpopmail";
	}

	for(var i=10100; i<=10199; i++){
		typefield[i]="FTS";
	}
	
	for(var i=11100; i<=11199; i++){
		typefield[i]="Ftpd";
	}

	for(var i=11200; i<=11299; i++){
		typefield[i]="ProFtpd";
	}

	for(var i=11300; i<=11399; i++){
		typefield[i]="Pure-Ftpd";
	}

	for(var i=11400; i<=11499; i++){
		typefield[i]="Vs-Ftpd";
	}

	for(var i=11500; i<=11599; i++){
		typefield[i]="MS-Ftp";
	}

	for(var i=12100; i<=12299; i++){
		typefield[i]="Named";
	}

	for(var i=13100; i<=13299; i++){
		typefield[i]="Samba";
	}

	for(var i=14100; i<=14199; i++){
		typefield[i]="Racoon SSL";
	}

	for(var i=14200; i<=14299; i++){
		typefield[i]="Cisco VPN Conc";
	}

	for(var i=17100; i<=17399; i++){
		typefield[i]="Policy";
	}

	for(var i=18100; i<=18499; i++){
		typefield[i]="Windows System";
	}

	for(var i=19100; i<=19199; i++){
		typefield[i]="VMWare";
	}

	for(var i=20100; i<=20299; i++){
		typefield[i]="IDS";
	}

	for(var i=20300; i<=20499; i++){
		typefield[i]="Snort IDS";
	}
	
	for(var i=30100; i<=30999; i++){
		typefield[i]="Apache";
	}

	for(var i=31100; i<=31199; i++){
		typefield[i]="Web Access";
	}
	
	for(var i=31200; i<=31299; i++){
		typefield[i]="Zeus WS";
	}

	for(var i=31300; i<=31399; i++){
		typefield[i]="Nginx";
	}
	
	for(var i=31400; i<=31499; i++){
		typefield[i]="Php";
	}
	
	for(var i=31500; i<=31599; i++){
		typefield[i]="Web Appsec";
	}
	
	for(var i=35000; i<=35999; i++){
		typefield[i]="Squid";
	}
	
	for(var i=40100; i<=40499; i++){
		typefield[i]="Attack Pattern";
	}
	
	for(var i=40500; i<=40599; i++){
		typefield[i]="Privilege Escalation";
	}
	
   	for(var i=40600; i<=40999; i++){
		typefield[i]="Scan Pattern";
	}

	for(var i=50100; i<=50299; i++){
		typefield[i]="MySQL";
	}

	for(var i=50500; i<=50799; i++){
		typefield[i]="PostgreSQL";
	}
	
	for(var i=51000; i<=51099; i++){
		typefield[i]="Dropbear";
	}
	
	for(var i=51500; i<=51599; i++){
		typefield[i]="OpenBSD";
	}

	for(var i=52000; i<=52099; i++){
		typefield[i]="Bro IDS";
	}

	for(var i=52500; i<=52599; i++){
		typefield[i]="Clam AV";
	}

	for(var i=100000; i<=100999; i++){
		typefield[i]="User Defined";
	}

	var jsonfy = JSON.stringify(
		{	component:"OSSEC Server", 
			object:json.component, 
			labels:[json.classification],
			type:typefield[json.id], 
			data:json, 
			timestamp: mseconds
		});
	
	var outjson = new Buffer(jsonfy.toString()+'\n');// se jsonfy è null, il client si chiude con un errore. \n è stato aggiunto come carattere di terminazione da far riconoscere ad hekad
	


	client.send(outjson, 0, outjson.length, PORTFWD, HOSTFWD, function(err, bytes) {
    		if (err) throw err;
    		console.log('UDP message sent to ' + HOSTFWD +':'+ PORTFWD);
    		client.close();
});


});

server.bind(PORT, HOST);



