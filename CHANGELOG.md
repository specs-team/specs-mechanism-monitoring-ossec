specs-monitoring-ossec CHANGELOG
================================

This file is used to list changes made in each version of the specs-monitoring-ossec cookbook.

0.1.0
-----
- [your_name] - Initial release of specs-monitoring-ossec

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
